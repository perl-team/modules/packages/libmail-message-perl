libmail-message-perl (3.016-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 3.016.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Dec 2024 00:00:52 +0100

libmail-message-perl (3.015-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 3.015.
    Closes: #1055597

 -- gregor herrmann <gregoa@debian.org>  Mon, 11 Dec 2023 17:29:22 +0100

libmail-message-perl (3.014-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 3.014.

 -- gregor herrmann <gregoa@debian.org>  Wed, 01 Nov 2023 22:17:42 +0100

libmail-message-perl (3.013-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 3.013.
  * Update years of upstream copyright.
  * Bump versioned test and runtime dependency on libuser-identity-perl.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Fri, 29 Sep 2023 03:16:59 +0200

libmail-message-perl (3.012-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
    * libmail-message-perl: Drop versioned constraint on libmail-box-perl
      in Replaces.
    * libmail-message-perl: Drop versioned constraint on libmail-box-perl
      in Breaks.

  [ gregor herrmann ]
  * Import upstream version 3.012.
  * Update years of upstream copyright.
  * Bump versioned (build) dependency on libuser-identity-perl.

 -- gregor herrmann <gregoa@debian.org>  Mon, 14 Feb 2022 18:21:00 +0100

libmail-message-perl (3.011-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libmailtools-perl.
    + libmail-message-perl: Drop versioned constraint on libmailtools-perl in
      Depends.

  [ gregor herrmann ]
  * Import upstream version 3.011.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Sep 2021 17:05:16 +0200

libmail-message-perl (3.010-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 3.010.
  * Make (build) dependency on libuser-identity-perl versioned.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Oct 2020 17:54:55 +0200

libmail-message-perl (3.009-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 3.009.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Sat, 08 Feb 2020 15:09:14 +0100

libmail-message-perl (3.008-2) unstable; urgency=medium

  * Team upload.
  * Bump versioned breaks on libmail-box-perl to << 3.006.
    Cf. #922333

 -- gregor herrmann <gregoa@debian.org>  Fri, 15 Feb 2019 12:23:10 +0100

libmail-message-perl (3.008-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 3.008.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.

 -- gregor herrmann <gregoa@debian.org>  Wed, 13 Feb 2019 20:05:08 +0100

libmail-message-perl (3.007-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 3.007.
  * Update upstream Contact.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Sun, 21 Oct 2018 22:24:34 +0200

libmail-message-perl (3.006-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update debian/upstream/metadata.
  * Update years of upstream copyright.
  * Drop pod-spelling.patch, applied upstream.
  * Drop some unneeded version constraints from (build) dependencies.
  * Turn off DNS queries during tests.

 -- gregor herrmann <gregoa@debian.org>  Wed, 14 Feb 2018 21:18:11 +0100

libmail-message-perl (3.005-1) unstable; urgency=low

  * Initial Release. Closes: #886027 (ITP)

 -- Damyan Ivanov <dmn@debian.org>  Mon, 01 Jan 2018 20:37:02 +0000
